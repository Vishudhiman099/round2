import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient, private router: Router) { }


  login(data:any){
    return this.http.post('http://localhost:3000/login',data).subscribe((res:any)=>{
      localStorage.setItem("token",res.token)
      this.router.navigate(['profile'])


    })
  }

  profile(){
    let headers = new HttpHeaders().set("Authorization",`bearer ${localStorage.getItem('token')}`)
    return this.http.post('http://localhost:3000/profile',{},{headers})
  }
}
