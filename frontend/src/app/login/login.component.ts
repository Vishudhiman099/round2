import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private fb : FormBuilder, private userService:UserService){}



  loginForm = this.fb.group({
    email:['', Validators.required],
    password:['', Validators.required]
    })



    userLogin(){
      this.userService.login(this.loginForm.value)

    }


}
