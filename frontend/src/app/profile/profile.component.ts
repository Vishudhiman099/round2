import { Component } from '@angular/core';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent {
  profileData: any;

  constructor(private userService:UserService){}


  ngOnInit(){
    this.userService.profile().subscribe((res)=>{
      if(res){
        this.profileData = res



      }
    })
  }
}
