const express = require("express");
const jwt = require("jsonwebtoken");
const bodyParser = require("body-parser")
const app = express();
const cors = require('cors')
const secretKey = "secretKey";
const cookieParser = require('cookie-parser');

app.use(cookieParser());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use(cors({origin:'http://localhost:4200'}))
app.use(bodyParser.json())

app.post("/login", (req, resp) => {
  const data = {
    id: 1,
    username: "Vishal",
    email: "vishudhiman099@gmail.com",
  };

  // jwt.sign({ data }, secretKey, { expiresIn: "300s" }, (err, token) => {
    
  //   resp.cookie('token', token, { httpOnly: true });
  //   resp.json({ token });

  // });

  const token = jwt.sign({ data }, secretKey, { expiresIn: '300s' });
  resp.cookie('token', token, { httpOnly: true });
  resp.json({
    message:"Logged in successfully",
    token: token
  });
});

app.post("/profile", verifyToken, (req, resp) => {
  // jwt.verify(req.token, secretKey, (err, authData) => {
  //   if (err) {
  //     console.log(err);
  //     resp.sendStatus(403)
     
  //   } else {
  //     resp.json({
  //       message: "Profile is accessable",
  //       authData,
  //     });
  //   }
  // });

  resp.send("Hi profile is accessable")
});


//this will work as a Middleware for profile page
function verifyToken(req, resp, next) {
  // const bearerHeader = req.headers['authorization']
  // if (typeof bearerHeader !== "undefined") {
  //   const bearer = bearerHeader.split(" ");
  //   const token = bearer[1];
  //   req.token = token;
  //   next();
  // } else {
  //   resp.send({
  //     result: "Token is not valid",
  //   });
  // }
  //const token = req.cookies.token;

  // if (!token) {
  //   return resp.status(401).send('Unauthorized');
  // }
  const bearerHeader = req.headers['authorization']
  const bearer = bearerHeader.split(" ");
  const token = bearer[1];
  if(token){
    
    jwt.verify(token,secretKey,(err,authdata)=>{
      if(err){
        resp.sendStatus(403)
      }
      else{
        resp.json({
          message:"Profile is OK",
          authdata : authdata,
          token:token
        })
      }
    })
  }
  else{
    return resp.status(401).json({
      message:"token is not given"
    });
  }
 
      
  
}

app.listen(3000, () => {
  console.log("Its running on 3000 port");
});
